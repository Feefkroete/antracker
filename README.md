# AnTracker
An Android application to monitor and document your ant colonies. It aims to make keeping ants more easy, entertaining, shareable, educative, etc.

## Roadmap
- [ ] Basic functionality (Choosing ant colonies, Create new ones)
- [ ] Information on ant colonies
- [ ] Journal editor
- [ ] Statistics
- [ ] ...

## Contributing
Create merge requests suggesting your changes. Please document your code and keep it readable.
To report Errors or issues please create a new issue after checking if it hadn't already been created.