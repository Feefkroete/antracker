package com.gitlab.feefkroete.antracker.ui

/**
 *  WARNING: This file is not at all done and should not be taken as reference! It is mainly used for backend testing at this time!
 */


import android.content.Context
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.BackHandler
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CutCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.gitlab.feefkroete.antracker.R
import com.gitlab.feefkroete.antracker.ui.theme.AnTrackerTheme
import com.gitlab.feefkroete.antracker.ui.theme.BoxDarkGray
import com.gitlab.feefkroete.antracker.ui.theme.BoxGray
import com.gitlab.feefkroete.antracker.util.*
import com.gitlab.feefkroete.antracker.util.database.AntDatabase
import com.gitlab.feefkroete.antracker.util.database.dateParser
import com.gitlab.feefkroete.antracker.util.file.FileIOHandler
import com.gitlab.feefkroete.antracker.util.file.generateMediaFileUri
import com.gitlab.feefkroete.antracker.util.file.getImageBitMapFromImageFile
import java.util.*


enum class UiState {
    MainMenu,
    ColonyDetails,
    ColonySetup
}

var currentUiState by mutableStateOf(UiState.MainMenu)

class MainActivity : ComponentActivity() {

    private val database by lazy {
        AntDatabase.getDatabase(applicationContext)
    }

    private val fileHandler = FileIOHandler(this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            AnTrackerTheme {
                //Render different layouts depending on the current UI state
                when (currentUiState) {
                    UiState.MainMenu -> {
                        Column {
                            Button(
                                onClick = {
                                    database.addColonyInfoToList(
                                        ColonyInfo(
                                            name = "Gerthas Grasse Gang",
                                            species = "Formica Rufa",
                                            queenNames = arrayOf("Layla","Gertha"),
                                            phase = ColonyPhase.TestTubeColony,
                                            gyneType = GyneType.Oligogynous,
                                            foundationType = FoundationType.Parasitic,
                                            origin = "Paris",
                                            foundationDate = Date(),
                                            profilePicturePath = null
                                        )
                                    )
                                //currentUiState = UiState.ColonySetup
                                }) {
                                Text(getString(R.string.new_colony))
                            }
                        }
                        //Lazy Column only renders items on the screen
                        LazyColumn(modifier = Modifier.fillMaxSize()) {
                            items(database.colonyInfoList) { currentColonyInfo ->
                                ColonyCard(currentColonyInfo, database)
                            }
                        }
                    }
                    UiState.ColonyDetails -> {
                        //Handles the action of the system back button.
                        BackHandler(enabled = true) {
                            currentUiState = UiState.MainMenu
                        }
                        AntJournalEntryScreen(database, applicationContext, null, fileHandler)
                    }
                    UiState.ColonySetup -> {
                        BackHandler(enabled = true) {
                            currentUiState = UiState.MainMenu
                        }
                        Column (
                            verticalArrangement = Arrangement.spacedBy(10.dp)
                        ) {
                            TopBar(getString(R.string.cancel), UiState.MainMenu)
                            val name = textInput(getString(R.string.colony_name), "Example Name")
                            val species = textInput(getString(R.string.species), "Lasius Niger")
                            val queenNames = textInput(getString(R.string.queen_names), "Esmeralda").split(",")
                            val gyneType = dropDownMenu(getString(R.string.gyny_type), arrayOf(getString(R.string.gyny_mono), getString(R.string.gyny_poly), getString(R.string.gyny_oligo)))
                            val foundationType = dropDownMenu(getString(R.string.foundation_type), arrayOf(getString(R.string.foundation_haplo), getString(R.string.foundation_pleo), getString(R.string.foundation_parasite)))
                            val phase = dropDownMenu(getString(R.string.colony_phase), arrayOf(getString(R.string.phase_foundation), getString(R.string.phase_testtube), getString(R.string.phase_mature), getString(R.string.phase_dead)))
                            val origin = textInput(getString(R.string.origin), "My backyard")
                            val foundationDate = textInput(getString(R.string.foundation_date), "YYYY-MM-DD")

                            Button(
                                onClick = {
                                    database.addColonyInfoToList(ColonyInfo(
                                        null,
                                        name,
                                        species,
                                        queenNames.toTypedArray(),
                                        enumValues<ColonyPhase>()[phase],
                                        enumValues<GyneType>()[gyneType],
                                        enumValues<FoundationType>()[foundationType],
                                        origin,
                                        dateParser.parse(foundationDate)!!,
                                        null
                                    ))
                                }
                            ){
                                Text(getString(R.string.okay))
                            }
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun ColonyCard(colony: ColonyInfo, database: AntDatabase) {
    Button(
        //modifier = Modifier.background(BoxGray),
        onClick = {
            database.getJournalEntriesForColony(colony.id!!)
            currentUiState = UiState.ColonyDetails
        },
        colors = ButtonDefaults.buttonColors(backgroundColor = BoxGray),
        contentPadding = PaddingValues(0.dp)
    ) {
        Row (modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 0.dp, vertical = 5.dp)) {
            Image(modifier = Modifier
                .padding(horizontal = 5.dp, vertical = 0.dp)
                .size(100.dp)
                .clip(CutCornerShape(20.dp)),
                painter = painterResource(R.drawable.default_ant),
                contentDescription = colony.name)
            Column (modifier = Modifier.height(100.dp), verticalArrangement = Arrangement.Center) {
                Text(text = colony.name, fontSize = 25.sp, fontWeight = FontWeight(500))
                Text(text = colony.species)
            }
        }
    }
}

@Composable
fun TopBar (title: String, futureUiState: UiState) {
    Button(
        onClick = { currentUiState = futureUiState },
        modifier = Modifier.fillMaxWidth().height(40.dp),
        colors = ButtonDefaults.buttonColors(backgroundColor = BoxGray),
        contentPadding = PaddingValues(0.dp)
    ) {
        Row (modifier = Modifier.fillMaxSize().background(Color.Transparent), verticalAlignment = Alignment.CenterVertically) {
            Icon(imageVector = Icons.Default.ArrowBack, contentDescription = "back", modifier = Modifier.size(40.dp))
            Text(title)
        }
    }
}

@Composable
fun textInput (label: String, placeholder: String) : String {
    var input by remember {
        mutableStateOf(TextFieldValue(""))
    }
    TextField(
        value = input,
        onValueChange = {
            input = it
        },
        modifier = Modifier.fillMaxWidth(),
        label = {
            Text(label, color = BoxDarkGray)
        },
        placeholder = {
            Text(placeholder, color = BoxGray)
        },
        colors = TextFieldDefaults.textFieldColors(BoxDarkGray)
    )
    return input.text
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun dropDownMenu (label: String, dropDownOptions: Array<String>) : Int {
    var expanded by remember { mutableStateOf(false) }
    var selectedOptionText by remember { mutableStateOf(label) }
    ExposedDropdownMenuBox(
        expanded = expanded,
        onExpandedChange = {
            expanded = !expanded
        }
    ) {
        TextField(
            readOnly = true,
            value = selectedOptionText,
            onValueChange = { },
            label = { Text(label) },
            trailingIcon = {
                ExposedDropdownMenuDefaults.TrailingIcon(
                    expanded = expanded
                )
            },
            colors = ExposedDropdownMenuDefaults.textFieldColors()
        )
        ExposedDropdownMenu(
            expanded = expanded,
            onDismissRequest = {
                expanded = false
            }
        ) {
            dropDownOptions.forEach { selectionOption ->
                DropdownMenuItem(
                    onClick = {
                        selectedOptionText = selectionOption
                        expanded = false
                    }
                ){
                    Text(text = selectionOption)
                }
            }
        }

    }
    return dropDownOptions.indexOf(selectedOptionText)

}

@Composable
fun AntJournalEntryScreen(database: AntDatabase, context: Context, colony: ColonyInfo?, fileIOHandler: FileIOHandler) {
    Column {
        TopBar("Back to main menu", UiState.MainMenu)
        Button(
            onClick = {
                val uri = generateMediaFileUri(context, "gertha", false)
                if (uri != null) {
                    fileIOHandler.takeImage(uri.first)
                    database.insertJournalEntry(
                        JournalEntry(date = Date(), colonyId = 1, images = arrayOf(uri.second.path))
                    )
                } else {
                    Toast.makeText(context, "There was an error creating the image file.", Toast.LENGTH_SHORT)
                        .show()
                }
            }) {
            Text("New Photo Entry")
        }
    }
    LazyColumn(modifier = Modifier.fillMaxSize()) {
        items(database.journalEntryList) { currentJournalEntry ->
            JournalEntryCard(currentJournalEntry, context)
        }
    }
}

@Composable
fun JournalEntryCard(journalEntry: JournalEntry, context: Context) {
    Button(
        modifier = Modifier.height(200.dp),
        onClick = {
            Toast.makeText(context, "Journal Entry details not implemented yet.", Toast.LENGTH_LONG).show()
        },
        colors = ButtonDefaults.buttonColors(backgroundColor = BoxGray),
        contentPadding = PaddingValues(0.dp)
    ) {
        LazyColumn (modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 0.dp, vertical = 5.dp)) {
            items (journalEntry.images) {currentImageUriString ->
                Image(modifier = Modifier
                    .padding(horizontal = 5.dp, vertical = 0.dp)
                    .size(180.dp),
                    bitmap = getImageBitMapFromImageFile(context, currentImageUriString),
                    contentDescription = journalEntry.colonyId.toString()
                )
            }
        }
    }
}

@Preview
@Composable
fun TopBarTest () {
    TopBar("Back to main menu", UiState.MainMenu)
}