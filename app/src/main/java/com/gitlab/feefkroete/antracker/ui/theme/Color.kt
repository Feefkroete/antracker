package com.gitlab.feefkroete.antracker.ui.theme

import androidx.compose.ui.graphics.Color

val BoxGray = Color(0xFF595959)
val BoxDarkGray = Color(0xFF454545)