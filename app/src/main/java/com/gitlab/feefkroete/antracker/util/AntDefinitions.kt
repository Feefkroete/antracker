package com.gitlab.feefkroete.antracker.util

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.Date

/**
 *  This Entity is stored in the 'colonies' table and contains all basic data of each colony
 *  @param id: Auto-generated id used in the database.
 *  @param name: The Name of the colony. MUST NOT be empty!
 *  @param species: The species of the colony.
 *  @param queenNames: The names of the queens.
 *  @param phase: The [ColonyPhase] of the colony.
 *  @param gyneType: The [GyneType] of the queen(s).
 *  @param foundationType: The [FoundationType] of the species.
 *  @param origin: The place the queen(s) was/were found / are coming from.
 *  @param foundationDate: The date on which the queen(s) started founding her/their new colony.
 *  @param profilePicturePath: The path to the profile picture of this colony.
 */
@Entity(tableName = "colonies")
data class ColonyInfo(
    @PrimaryKey(autoGenerate = true)
    val id: Int? = null,
    val name: String,
    val species: String,
    val queenNames: Array<String>,
    val phase: ColonyPhase,
    val gyneType: GyneType,
    val foundationType: FoundationType,
    val origin: String,
    val foundationDate: Date,
    val profilePicturePath: String?
)

/**
 *  This Entity is stored in the 'journal' table and contains one journal entry.
 *  @param id: Auto-generated id used in the database.
 *  @param date: The date the JournalEntry was created.
 *  @param colonyId: The id of the [ColonyInfo] this JournalEntry belongs to.
 *  @param foodLevel: The new food level after caring for the colony. This should be given in percentage points from 0 to 100. A value of -1 signals no change from previous value.
 *  @param waterLevel: The new water level after caring for the colony. This should be given in percentage points from 0 to 100. A value of -1 signals no change from previous value.
 *  @param eggsAmt: The new amount of eggs present in the colony. A value of -1 signals no change from previous value.
 *  @param larvaeAmt: The new amount of larvae present in the colony. A value of -1 signals no change from previous value.
 *  @param pupaeAmt: The new amount of pupae present in the colony. A value of -1 signals no change from previous value.
 *  @param workerBehavior: The current [WorkerBehavior] of the workers in the colony. Defaults to [WorkerBehavior.NoEntry].
 *  @param notes: A String of notes the user can add to each journal entry.
 *  @param images: Array of Strings containing paths to images which should be displayed in this journal entry.
 */
@Entity(tableName = "journal")
data class JournalEntry(
    @PrimaryKey(autoGenerate = true)
    val id: Int? = null,
    val date: Date,
    val colonyId: Int,
    val foodLevel: Byte = -1,
    val waterLevel: Byte = -1,
    val eggsAmt: Int = -1,
    val larvaeAmt: Int = -1,
    val pupaeAmt: Int = -1,
    val workerBehavior: WorkerBehavior = WorkerBehavior.NoEntry,
    val notes: String = "",
    val images: Array<String> = emptyArray()
)

/**
 *  Enum to represent the current phase of a colony.
 *  @property Foundation: The queen(s) currently don't have any workers; Foundation is starting.
 *  @property TestTubeColony: Could be renamed to Young Colony, as not every colony will start in a test tube setup. There are already some workers,
 *  but busyness hasn't really started yet.
 *  @property Colony: The colony is big and there's a lot going on now.
 *  @property Dead: The colony/queen has died :(  Don't worry, you'll find another queen :P
 */
enum class ColonyPhase {
    Foundation,
    TestTubeColony,
    Colony,
    Dead
}

/**
 *  Enum to represent the amount of queens in a colony. See [AntWiki](https://www.antwiki.org/wiki/Polygyny).
 *  @property Monogynous: The colony only has one egg laying queen.
 *  @property Polygynous: The colony has multiple queens. This number can reach from a few to millions of queens.
 *  @property Oligogynous: The colony has multiple queens which live in separate rooms as they might kill each other if they meet in the colony.
 */
enum class GyneType {
    Monogynous,
    Polygynous,
    Oligogynous
}

/**
 *  Enum to represent the way the queen(s) are founding their colony. See [AntWiki](https://www.antwiki.org/wiki/Colony_Foundation#Two_Contrasting_Strategies_of_Colony_Foundation)
 *  @property Claustral: The queen(s) don't have to leave the nest during foundation.
 *  @property Semiclaustral: The queen(s) leave the nest during foundation as they need food.
 *  @property Parasitic: The queen(s) live as parasitic member of another colony.
 *  @property Fork: The queen(s) split off of the old colony with some workers to found a new colony.
 */
enum class FoundationType {
    Claustral,
    Semiclaustral,
    Parasitic,
    Fork
}

/**
 *  Enum to represent the behaviour of the workers of the colony. This might be used to predict hibernation.
 *  @property Aggressive: Workers react aggressively to any disturbance.
 *  @property VeryActive: Workers are very active and do a lot of work.
 *  @property Active: Workers are active and carry out basic tasks.
 *  @property LittleActive: Workers only carry out important tasks.
 *  @property Passive: Workers are passive, could be because of hibernation.
 *  @property Inactive: Workers don't react to anything, something might be wrong.
 *  @property NoEntry: No entry for WorkerBehaviour
 */
enum class WorkerBehavior {
    Aggressive,
    VeryActive,
    Active,
    LittleActive,
    Passive,
    Inactive,
    NoEntry
}