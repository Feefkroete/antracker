package com.gitlab.feefkroete.antracker.util

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.result.contract.ActivityResultContracts
import com.gitlab.feefkroete.antracker.R

/**
 *  A helper class for managing permissions.
 */
class PermissionHelper (activity: ComponentActivity) {
    /** Lambda function containing the code which should be executed when the camera permission is granted. Will be set by [requestCameraPermission]. */
    private var onCameraPermissionGranted: () -> Unit = {}
    
    /** 
     *  The RequestPermission ActivityResultContract is registered. In case of success [onCameraPermissionGranted] will be called. Else, a Toast will
     *  be created saying that the camera can't be used without permission.
     */
    private val permissionLauncher = activity.registerForActivityResult(ActivityResultContracts.RequestPermission()) { success ->
        if (success) {
            onCameraPermissionGranted()
        } else {
            Toast.makeText(activity.applicationContext, activity.getString(R.string.camera_permission_denied_toast), Toast.LENGTH_SHORT).show()
        }
    }

    /**
     *  Checks if camera permission has been granted.
     *  @return True if the permission has already been granted.
     */
    fun checkCameraPermission(context: Context): Boolean {
        return context.checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
    }

    /**
     *  Requests camera permissions. This opens the android dialog for camera permissions.
     *  @param onPermissionGranted: Lambda function which will be called if the camera permission has been granted.
     */
    fun requestCameraPermission(onPermissionGranted: () -> Unit) {
        this.onCameraPermissionGranted = onPermissionGranted
        permissionLauncher.launch(Manifest.permission.CAMERA)
    }
}