package com.gitlab.feefkroete.antracker.util.database

import android.annotation.SuppressLint
import androidx.room.TypeConverter
import com.gitlab.feefkroete.antracker.util.ColonyPhase
import com.gitlab.feefkroete.antracker.util.FoundationType
import com.gitlab.feefkroete.antracker.util.GyneType
import com.gitlab.feefkroete.antracker.util.WorkerBehavior
import java.text.SimpleDateFormat
import java.util.*

@SuppressLint("SimpleDateFormat")
val dateParser = SimpleDateFormat("yyyy-MM-dd")

/**
 *  Helper Class for the Room database. Provides functions to convert objects/enum values/etc. to database-compatible Strings/Ints
 */
class Converter {
    @TypeConverter
    fun fromColonyPhase(phase: ColonyPhase) = phase.ordinal
    @TypeConverter
    fun toColonyPhase(phase: Int) = enumValues<ColonyPhase>()[phase]
    @TypeConverter
    fun fromGyneType(type: GyneType) = type.ordinal
    @TypeConverter
    fun toGyneType(type: Int) = enumValues<GyneType>()[type]
    @TypeConverter
    fun fromFoundationType(type: FoundationType) = type.ordinal
    @TypeConverter
    fun toFounationType(type: Int) = enumValues<FoundationType>()[type]
    @TypeConverter
    fun fromWorkerBehavior(behavior: WorkerBehavior) = behavior.ordinal
    @TypeConverter
    fun toWorkerBehavior(behavior: Int) = enumValues<WorkerBehavior>()[behavior]
    @TypeConverter
    fun fromDate(date: Date) = dateParser.format(date)
    @TypeConverter
    fun toDate(date: String) = dateParser.parse(date)
    @TypeConverter
    fun fromStringArray(array: Array<String>) : String {
        //Returns a string of comma separated Strings. ["Mathilda", "Layla", "Roxanne", "Pauli"] -> "Mathilda,Layla,Roxanne,Pauli"
        var outputString = ""
        array.map { name ->
            if (!name.contains(",")) {  //Check if the name contains a comma. This shouldn't happen but is checked here for safety.
                outputString += "$name,"
            }
        }
        return outputString.dropLast(1)     //Remove last comma
    }
    @TypeConverter
    fun toStringArray(array: String) : Array<String> {
        return array.split(",").toTypedArray()
    }
}