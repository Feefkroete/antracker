package com.gitlab.feefkroete.antracker.util.database

import android.content.Context
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.room.*
import com.gitlab.feefkroete.antracker.util.ColonyInfo
import com.gitlab.feefkroete.antracker.util.JournalEntry
import kotlinx.coroutines.launch

class AntDatabase private constructor(private val database: AppDatabase) : ViewModel() {
    companion object {
        private var antDatabaseInstance: AntDatabase? = null

        /**
         *  Returns an Instance of the AntDatabase class. This acts as a Singleton to ensure only one database instance is created.
         */
        fun getDatabase(context: Context): AntDatabase {
            if (antDatabaseInstance == null) {
                return AntDatabase(
                    Room.databaseBuilder (
                        context,
                        AppDatabase::class.java, "antDatabase"
                    ).build()
                )
            }
            return antDatabaseInstance!!
        }
    }
    
    //      --- ColonyInfo related methods ---      //

    /**
     *  Contains all basic ColonyInfo objects which are displayed e.g. in the main menu.
     *  - Use [getColonyListFromDatabase] update this list from the database.
     *  - Use [addColonyInfoToList] to add a colony to this list and the database.
     *  @see [deleteColonyInfo]
     */
    var colonyInfoList: List<ColonyInfo> by mutableStateOf(
        ArrayList()
    )
    init {
        //Read the database into colonyInfoList on initialization
        getColonyListFromDatabase()
    }

    /** Update the [colonyInfoList] from the database. */
    fun getColonyListFromDatabase() {
        viewModelScope.launch {
            colonyInfoList = database.colonyInfoDao().getAll()
        }
    }
    /** Add a new colony to the [colonyInfoList] and the database. */
    fun addColonyInfoToList(colony: ColonyInfo) {
        colonyInfoList += colony
        viewModelScope.launch {
            database.colonyInfoDao().insert(colony)
        }
        //TODO: Implement better way to make newest added colony have an auto generated id (used when querying for journal entries for a specific colony)
        getColonyListFromDatabase()
    }
    /** 
     *  Delete a colonyInfo from the database.
     *  @param colony: The [ColonyInfo] to be deleted
     *  @param deleteJournalEntries: True if all journal entries belonging to the specified ColonyInfo should also be deleted.
     */
    fun deleteColonyInfo(colony: ColonyInfo, deleteJournalEntries: Boolean = true) {
        if (deleteJournalEntries) {
            viewModelScope.launch {
                deleteAllJournalEntriesOfColony(colony.id!!)
            }
        }
        colonyInfoList.drop(colonyInfoList.indexOf(colony))
        viewModelScope.launch { 
            database.colonyInfoDao().delete(colony)
        }
    }

    //      --- JournalEntry related methods ---      //

    /**
     *  Contains JournalEntry objects which were returned by the database.
     *
     *  To get a specified amount of entries for some colony, use [getJournalEntriesForColony].
     *  @see [insertJournalEntry]
     *  @see [deleteJournalEntry]
     */
    var journalEntryList: List<JournalEntry> by mutableStateOf(
        ArrayList()
    )

    /**
     *  Read a specified amount of JournalEntry Entities concerning the specified colony from the 'journal' table.
     *
     *  The returned List will be ordered like this:
     *  ```
     *  [oldest entry, ..., newest entry until limit]
     *  ```
     *  The colonyId must be [JournalEntry.id].
     */
    fun getJournalEntriesForColony(colonyId: Int, limit: Short = 30) {
        viewModelScope.launch {
            journalEntryList = database.journalEntryDao().getJournalEntriesForColony(colonyId, limit)
        }
        journalEntryList = emptyList()
    }
    
    /**
     *  Insert a new JournalEntry into the database
     */
    fun insertJournalEntry(journalEntry: JournalEntry) {
        journalEntryList += journalEntry
        viewModelScope.launch {
            database.journalEntryDao().insert(journalEntry)
        }
    }

    /** Delete a JournalEntry object from [journalEntryList] and the database. */
    fun deleteJournalEntry(journalEntry: JournalEntry) {
        journalEntryList.drop(journalEntryList.indexOf(journalEntry))
        viewModelScope.launch {
            database.journalEntryDao().delete(journalEntry)
        }
    }
    
    /**
     *  Deletes all [journal entries][JournalEntry] of one colony. This should also be called when deleting a colony to avoid obsolete data.
     *  @see [deleteColonyInfo]
     */
    fun deleteAllJournalEntriesOfColony(colonyId: Int) {
        journalEntryList = ArrayList()
        viewModelScope.launch {
            database.journalEntryDao().deleteAllJournalEntriesOfColony(colonyId.toString())
        }
    }
}
@Dao
interface ColonyInfoDao {
    //  --- Query --- //
    /**
     *  Get a list of [ColonyInfo] data objects from the 'colonies' table of the database.
     */
    @Query("SELECT * FROM colonies")
    suspend fun getAll(): List<ColonyInfo>

    /**
     *  Get a specific [ColonyInfo] by id from the 'colonies' table of the database.
     */
    @Query("SELECT * FROM colonies WHERE id = :id")
    suspend fun getById(id: Int): ColonyInfo
    //  --- Insert --- //
    /**
     *  Insert a new [ColonyInfo] into the 'colonies' table of the database.
     */
    @Insert
    suspend fun insert(colonyInfo: ColonyInfo)

    //  --- Update --- //
    /**
     * Update the specified [colonyInfo], e.g. for changing the colony status
     */
    @Update
    suspend fun update(colonyInfo: ColonyInfo)

    //  --- Delete --- //
    /**
     *  Delete a [ColonyInfo] from the 'colonies' table of the database. This does not delete the logs of the colony in the 'logs' table!
     */
    @Delete
    suspend fun delete(colonyInfo: ColonyInfo)
    
    /** Delete a [ColonyInfo] by id. */
    @Query("DELETE FROM colonies WHERE id = :id")
    suspend fun deleteById(id: Int)
}

@Dao
interface JournalEntryDao{
    //  --- Query --- //
    /** Get a specified amount of [journalEntries][JournalEntry] of a specified colony. */
    @Query("SELECT * FROM journal WHERE colonyId = :colonyId ORDER BY date DESC LIMIT :limit")
    suspend fun getJournalEntriesForColony(colonyId: Int, limit: Short): List<JournalEntry>

    /** Get a single [JournalEntry] by id. Can return null if no JournalEntry has the specified id. */
    @Query("SELECT * FROM journal WHERE id = :id LIMIT 1")
    suspend fun getJournalEntryById(id: Int): JournalEntry?

    //  --- Insert --- //
    /** Insert a [JournalEntry] into the database. */
    @Insert
    suspend fun insert(journalEntry: JournalEntry)

    //  --- Update --- //
    /** Update a specified [JournalEntry] (e.g. when editing its contents) */
    @Update
    fun update(journalEntry: JournalEntry)

    //  --- Delete --- //
    /** Delete a [JournalEntry] from the database. */
    @Delete
    suspend fun delete(journalEntry: JournalEntry)

    /** Delete a [JournalEntry] from the database by id. */
    @Query("DELETE FROM journal WHERE colonyId = :colonyId")
    suspend fun deleteAllJournalEntriesOfColony(colonyId: String)
}

@TypeConverters(Converter::class)
@Database(entities = [ColonyInfo::class, JournalEntry::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun colonyInfoDao(): ColonyInfoDao
    abstract fun journalEntryDao(): JournalEntryDao
}