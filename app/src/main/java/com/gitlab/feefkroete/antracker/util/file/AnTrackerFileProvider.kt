package com.gitlab.feefkroete.antracker.util.file

import androidx.core.content.FileProvider
import com.gitlab.feefkroete.antracker.R

class AnTrackerFileProvider : FileProvider(R.xml.file_paths)