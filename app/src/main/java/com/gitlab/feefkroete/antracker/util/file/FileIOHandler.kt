package com.gitlab.feefkroete.antracker.util.file

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Environment
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.asImageBitmap
import androidx.core.content.FileProvider
import com.gitlab.feefkroete.antracker.R
import com.gitlab.feefkroete.antracker.util.PermissionHelper
import java.io.File
import java.io.FileNotFoundException
import java.text.SimpleDateFormat
import java.util.*

const val AUTHORITY = "com.gitlab.feefkroete.antracker.provider"
const val FILE_PATH_IMAGES_INTERNAL = "images"
const val FILE_PATH_VIDEOS_INTERNAL = "videos"
const val FILE_PATH_CACHE_INTERNAL = "cache"
const val FILE_PATH_IMAGES_EXTERNAL = "images_external"
const val FILE_PATH_VIDEOS_EXTERNAL = "videos_external"
const val FILE_PATH_CACHE_EXTERNAL = "cache_external"
const val IMAGE_FILE_EXTENSION = ".jpg"
const val VIDEO_FILE_EXTENSION = ".mp4"
const val PREFER_EXTERNAL_STORAGE = true

/**
 *  Handles file IO by providing methods for easy file/camera/gallery interaction.
 */
class FileIOHandler(private val activity: ComponentActivity) {
    /** Instance of [PermissionHelper] for requesting and checking permission to files/camera. */
    private val permissionHelper = PermissionHelper(activity)

    /**
     *  ActivityResult registration for taking pictures using the camera app. This also invokes the specified callback function
     *  when the camera app is closed (by exiting or taking a photo). If a picture has been saved successfully [onSuccess] will
     *  be called, else [onNonSuccess] will be called.
     */
    private val cameraTakePictureLauncher = activity.registerForActivityResult(ActivityResultContracts.TakePicture()) { success ->
        if(success) {
            onSuccess()
        } else {
            onNonSuccess()
        }
    }
    
    /**
     *  ActivityResult registration for capturing videos using the camera app. This also invokes the specified callback function
     *  when the camera app is closed (by exiting or capturing a video). If a video has been saved successfully [onSuccess] will
     *  be called, else [onNonSuccess] will be called.
     */
    private val cameraCaptureVideoLauncher = activity.registerForActivityResult(ActivityResultContracts.CaptureVideo()) { success ->
        if(success) {
            onSuccess()
        } else {
            onNonSuccess()
        }
    }
    
    /** 
     *  Lambda function which will be called by the callback functions ([cameraCaptureVideoLauncher], [cameraCaptureVideoLauncher]) returns a success. Will be set by 
     *  [takeImage] and [captureVideo].
     */
    private var onSuccess: () -> Unit = {}

    /**
     *  Lambda function which will be called by the callback functions ([cameraCaptureVideoLauncher], [cameraCaptureVideoLauncher]) returns a non-success. Will be set by
     *  [takeImage] and [captureVideo].
     */
    private var onNonSuccess: () -> Unit = {}

    /**
     *  Arranges everything to take a picture using the camera app. This includes checking and, if necessary, requesting permissions.
     *  Launches the [cameraTakePictureLauncher] and returns to the UI. The callback will be executed separately.
     *  @param uri: Uri in which the photo should be saved. It should be generated by [generateMediaFileUri].
     *  @param onPictureTaken: Lambda function which contains code which should be executed when the picture had been saved successfully.
     *  @param onNoPictureTaken: Lambda function which contains code which should be executed when no picture had been saved.
     *  @see [captureVideo]
     */
    fun takeImage(uri: Uri, onPictureTaken: () -> Unit = {}, onNoPictureTaken: () -> Unit = {}) {
        onSuccess = onPictureTaken
        onNonSuccess = onNoPictureTaken
        if (permissionHelper.checkCameraPermission(activity.applicationContext)) {
            cameraTakePictureLauncher.launch(uri)
        } else {
            permissionHelper.requestCameraPermission {
                cameraTakePictureLauncher.launch(uri)
            }
        }
    }
    
    /**
     *  Arranges everything to capture a video using the camera app. This includes checking and, if necessary, requesting permissions.
     *  Launches the [cameraCaptureVideoLauncher] and returns to the UI. The callback will be executed separately.
     *  @param uri: Uri in which the video should be saved. It should be generated by [generateMediaFileUri].
     *  @param onVideoTaken: Lambda function which contains code which should be executed when the video had been saved successfully.
     *  @param onNoVideoTaken: Lambda function which contains code which should be executed when no video had been saved.
     *  @see [takeImage]
     */
    fun captureVideo(uri: Uri, onVideoTaken: () -> Unit = {}, onNoVideoTaken: () -> Unit = {}) {
        onSuccess = onVideoTaken
        onNonSuccess = onNoVideoTaken
        if (permissionHelper.checkCameraPermission(activity.applicationContext)) {
            cameraCaptureVideoLauncher.launch(uri)
        } else {
            permissionHelper.requestCameraPermission {
                cameraCaptureVideoLauncher.launch(uri)
            }
        }
    }
}

/**
 *  Generates a new Uri for an image or a video. This can be used in an ActivityResultContract to specify the storage location.
 *
 *  The Uri is assembled as follows:
 *  ```
 *  [internal/external video/image directory] [the colony name] [the date and time rounded to seconds] [image/video file extension]
 *  ```
 *  @param context: The application Context.
 *  @param colonyName: The name of the colony which the future image/video is corresponding to.
 *  @param isVideo: True if the media is going to be a video, false if it's going to be an image.
 *  @return Pair of the generated Uri and the corresponding File. Returns null if there was an error creating the File.
 *
 *  @see [androidx.activity.result.contract.ActivityResultContracts.TakePicture]
 *  @see [androidx.activity.result.contract.ActivityResultContracts.CaptureVideo]
 *  @see [PREFER_EXTERNAL_STORAGE]
 *  @see [getImageBitMapFromImageFile]
 */
@SuppressLint("SimpleDateFormat")
fun generateMediaFileUri(context: Context, colonyName: String, isVideo: Boolean): Pair<Uri, File>? {
    val willSaveExternally = PREFER_EXTERNAL_STORAGE && isExternalStorageAvailable(false)   //Save on external storage if possible and wished so by user
    val parentDir = File(
        if(willSaveExternally) context.getExternalFilesDir(null) else context.filesDir,
        if (willSaveExternally) {
            if(isVideo) {
                FILE_PATH_VIDEOS_EXTERNAL
            } else {
                FILE_PATH_IMAGES_EXTERNAL
            }
        } else {
            if(isVideo) {
                FILE_PATH_VIDEOS_INTERNAL
            } else {
                FILE_PATH_IMAGES_INTERNAL
            }
        }
    )
    parentDir.mkdirs()  //Make sure the folder exists to avoid a FileNotFoundException when trying to create a file in this directory
    val dateParser = SimpleDateFormat("yyyy-MM-dd_HH-mm-ss")
    val fileNameDate = dateParser.format(Date())
    return try {
        val mediaFile = File(
            parentDir,
            "${generateFat32CompatibleFileName(colonyName)}_${fileNameDate}${if (isVideo) VIDEO_FILE_EXTENSION else IMAGE_FILE_EXTENSION}"
        )
        Pair(FileProvider.getUriForFile(context, AUTHORITY, mediaFile), mediaFile)
    } catch (e: FileNotFoundException) {
        Log.e("generateMediaFile", "Could not create Media File! \n${e.stackTrace}")
        null
    }
}

/**
 *  Gets a ImageBitmap from the specified image path. This must be a path starting with context.filesDir or context.getExternalFilesDir(null)
 *  
 *  Returns a default ImageBitmap if the path didn't contain a valid image.
 *  @param context: The application context
 *  @param path: The path of the image.
 *  @see [generateMediaFileUri]
 */
@Deprecated("Image will be loaded im UI thread which is bad for performance! Use Lazy image loading instead!")
fun getImageBitMapFromImageFile(context: Context, path: String) : ImageBitmap {
    val bitmap: Bitmap?
    try {
        bitmap = BitmapFactory.decodeFile(path)
    } catch (_: FileNotFoundException) {
        Log.w("BitmapFromFile", "Could not find image $path. Using default image instead.")
        return BitmapFactory.decodeResource(context.resources, R.drawable.default_ant).asImageBitmap()
    }
    if (bitmap != null) {
        return bitmap.asImageBitmap()
    }
    Log.w("BitmapFromFile", "Could not decode image $path. Using default image instead.")
    return BitmapFactory.decodeResource(context.resources, R.drawable.default_ant).asImageBitmap()
}

/**
 *  Checks if external storage (e.g. sdcard) is available. Returns true if it is read/writable or, 
 *  if [readonly] has been set to true, also when it is only mounted as readable. 
 *  @param readonly: True if only reading is necessary and writing is not needed.
 */
fun isExternalStorageAvailable(readonly: Boolean): Boolean {
    return when (Environment.getExternalStorageState()) {
        Environment.MEDIA_MOUNTED -> true   //External storage is mounted as read/writable
        Environment.MEDIA_UNMOUNTED -> {
            //TODO: Maybe suggest mounting sdcard if unmounted
            false
        }
        Environment.MEDIA_MOUNTED_READ_ONLY -> {
            //TODO: Block new media creation if external storage is mounted readonly.
            readonly //Returns true if only reading is required.
        }
        else -> false
    }
}

/**
 *  Removes all characters from the string which are not digits, letters or '&'. Replaces whitespaces with underscores.
 *  @param rawString: The String to be sanitized.
 */
fun generateFat32CompatibleFileName(rawString: String): String {
    var noLeadingWhitespaceString = rawString
    while (noLeadingWhitespaceString.startsWith(" ")) {
        noLeadingWhitespaceString = rawString.drop(1)
    }
    var outputString = ""
    for(c in noLeadingWhitespaceString) {
        if (c.isLetterOrDigit() || c == '&'){
            outputString += c
        }
        if (c == ' ') {
            outputString += '_'
        }
    }
    if (outputString == "") {
        outputString = "no_name"
    }
    if (outputString.length >= 100) {
        outputString = outputString.substring(0, 100)
    }
    return outputString
}
